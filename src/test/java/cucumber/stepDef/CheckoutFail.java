package cucumber.stepDef;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;

public class CheckoutFail {
    WebDriver driver;
    String baseUrl = "https://www.saucedemo.com/";

    @Given("user in cart page")
    public void user_in_cart_page(){
        WebDriverManager.chromedriver().setup();
        ChromeOptions opt = new ChromeOptions();

        driver = new ChromeDriver(opt);
        driver.manage().timeouts().implicitlyWait(60,TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(baseUrl);
        driver.findElement(By.id("user-name")).sendKeys("standard_user");
        driver.findElement(By.id("password")).sendKeys("secret_sauce");
        driver.findElement(By.xpath("//input[@type='submit']")).click();
        driver.findElement(By.xpath("//button[@id='add-to-cart-sauce-labs-backpack']")).click();
        driver.findElement(By.xpath("//a[@class='shopping_cart_link']")).click();
    }
    @When("user click checkout button")
    public void user_click_checkout_button(){
        driver.findElement(By.xpath("//button[@id='checkout']")).click();
    }
    @Then("in checkout step one")
    public void in_checkout_step_one(){
        driver.findElement(By.xpath("//span[contains(text(), 'Checkout: Your Information')]")).click();
    }
    @When("user input valid firstname")
    public void user_input_valid_firstname(){
        driver.findElement(By.id("first-name")).sendKeys("Andika");
    }
    @And("user input valid lastname")
    public void user_not_input_lastname(){
        driver.findElement(By.id("last-name")).sendKeys("Dwiputra");
    }
    @And("user not input zipcode")
    public void user_not_input_zipcode(){
        driver.findElement(By.id("postal-code")).sendKeys("");
    }
    @And("user click continue button")
    public void user_click_continue_button(){
        driver.findElement(By.xpath("//input[@id='continue']")).click();
    }
    @Then("error message appeared")
    public void error_message_appeared(){
        String errorCheckout = driver.findElement(By.xpath("//h3[@data-test='error']")).getText();
        Assert.assertEquals(errorCheckout, "Error: Postal Code is required");
        driver.close();
    }
}
