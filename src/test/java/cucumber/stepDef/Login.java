package cucumber.stepDef;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;

public class Login {
    WebDriver driver;
    String baseUrl = "https://www.saucedemo.com/";

    @Given("user in login page")
    public void user_in_login_page(){
        WebDriverManager.chromedriver().setup();
        ChromeOptions opt = new ChromeOptions();

        driver = new ChromeDriver(opt);
        driver.manage().timeouts().implicitlyWait(60,TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(baseUrl);
    }
    @When("user input valid username")
    public void user_input_valid_username(){
        driver.findElement(By.id("user-name")).sendKeys("standard_user");
    }
    @And("user input valid password")
    public void user_input_valid_password(){
        driver.findElement(By.id("password")).sendKeys("secret_sauce");
    }
    @And("user click button login")
    public void user_click_button_login(){
        driver.findElement(By.xpath("//input[@type='submit']")).click();
    }
    @Then("user redirect to product page")
    public void user_redirect_to_product_page(){
        String product_page = driver.findElement(By.xpath("//span[contains(text(),'Products')]")).getText();
        Assert.assertEquals(product_page, "Products");
        driver.close();
    }
}
