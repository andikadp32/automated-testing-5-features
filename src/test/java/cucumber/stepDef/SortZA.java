package cucumber.stepDef;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;

public class SortZA {
    WebDriver driver;
    String baseUrl = "https://www.saucedemo.com/";

    @Given("user is on products page")
    public void user_already_login(){
        WebDriverManager.chromedriver().setup();
        ChromeOptions opt = new ChromeOptions();

        driver = new ChromeDriver(opt);
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(baseUrl);
        driver.findElement(By.id("user-name")).sendKeys("standard_user");
        driver.findElement(By.id("password")).sendKeys("secret_sauce");
        driver.findElement(By.xpath("//input[@type='submit']")).click();
    }
    @When("user click sort button")
    public void user_click_sort_button(){
        String sort_default = driver.findElement(By.xpath("//option[contains(text(),'Name (A to Z)')]")).getText();
        Assert.assertEquals(sort_default, "Name (A to Z)");
    }
    @And("choose name ZA")
    public void choose_name_ZA(){
        driver.findElement(By.xpath("//option[contains(text(),'Name (Z to A)')]")).click();
    }
    @Then("products sorted by name ZA")
    public void products_sorted_by_name_ZA(){
        String sort_default = driver.findElement(By.xpath("//option[contains(text(),'Name (Z to A)')]")).getText();
        Assert.assertEquals(sort_default, "Name (Z to A)");
        driver.close();
    }
}
